var giswisata_db = require('../model/db');

exports.getSave_giswisata = function setSave_giswisata(data, callback) {

    var gis_temp = {
        nama_tempat : data.nama_tempat,
        daerah : data.daerah,
        kota : data.kota,
        deskripsi : data.deskripsi,
        langtitude : data.langtitude,
        longtitude : data.longtitude,
        image_name : data.image_name
    };

    var _mgiswisata = new giswisata_db.giswisata_entity(gis_temp);

    _mgiswisata.save(function (error, data) {
        if (error) {
            callback(error);
        } else {
            console.log("Added New Gis Wisata");
            callback(data);
        }
    });
};

exports.getFindById_giswisata = function setFindById_giswisata(parseData, callback) {
    var _giswisata = giswisata_db.giswisata_entity;
    _giswisata.find({ _id: parseData }, function (error, data) {
        if (data == '[]') {
            callback('not found');
        } else {
            callback(data);
        }
    });
}

exports.getDel_giswisata = function setDel_giswisata(parseData,callback){
    var _mgiswisata = giswisata_db.giswisata_entity;
    _mgiswisata.find({_id: parseData}, function (error,data) {
        if (data) {
            console.log('ini dia',data[0]._id)
            _mgiswisata.remove({_id:data[0]._id},function(error){
                if (!error) {
                    console.log("delete data berhasil",error);
                    callback(error);
                } else {
                    console.log("delete data tidak berhasil",error);
                    callback(error);
                }
            })
        }   
    });
}


exports.getUpdate_giswisata = function setUpdate_giswisata(data,callback){

   var temp_giswisata = {
        nama_tempat : data.nama_tempat,
        daerah : data.daerah,
        kota : data.kota,
        deskripsi : data.deskripsi,
        langtitude : data.langtitude,
        longtitude : data.longtitude,
        image_name : data.image_name
    };
   
    var _mgiswisata = giswisata_db.giswisata_entity; //kalo update jangan pake new
   
   _mgiswisata.update({_id:data._id},temp_giswisata,{upsert: true},function(error){
        callback(error);
   });
}

exports.getAll_giswisata = function setAll_giswisata(callback){
    var _mgiswisata = giswisata_db.giswisata_entity;//new doa_wanita.doa_wanita_entity(doa_wanita_data);
    _mgiswisata.find({},function (error, data) {
            if (data) {
                console.log(data);
                callback(data)
            }else{
                callback(null);
                data = null;
            }
        });
}