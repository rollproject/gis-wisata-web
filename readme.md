##### Dokumentasi domain giswisata-vinovino.rhcloud.com
######openshift.redhat.com

access : [](http://)http://giswisata-vinovino.rhcloud.com
email : vino.giswisata@live.com 
######GIT
```markdown
git clone ssh://52b1c8b45973cae179000059@giswisata-vinovino.rhcloud.com/~/git/giswisata.git/
cd giswisata/

```

```markdown
git add .
git commit -m 'My changes'
git push
```
######mongodb
```markdown
MongoDB 2.2 database added.  Please make note of these credentials:

   Root User:     admin
   Root Password: 7RBTFnJibX3n
   Database Name: giswisata

Connection URL: mongodb://$OPENSHIFT_MONGODB_DB_HOST:$OPENSHIFT_MONGODB_DB_PORT/

```

######Managing your cartridge ######

Most of the capabilities of OpenShift are exposed through our command line tool, rhc. Whether it's adding cartridges, checking uptime, or pulling log files from the server, you can quickly put a finger on the pulse of your application. Follow these steps to install the client on Linux, Mac OS X, or Windows.

You can get a list of cartridges by running

```markdown
rhc cartridge list
```
######You can manage the cartridge by running one of these commands

```markdown
rhc cartridge start -a giswisata -c mongodb-2.2
rhc cartridge stop -a giswisata -c mongodb-2.2
rhc cartridge restart -a giswisata -c mongodb-2.2
rhc cartridge reload -a giswisata -c mongodb-2.2
rhc cartridge status -a giswisata -c mongodb-2.2

```


You can remove the cartridge by running the following command.

Warning: make sure you've backed up any data you wish to keep before running this command

```markdown
rhc cartridge remove -a giswisata -c mongodb-2.2

```

