var mongoose = require('mongoose');
var fs = require('fs');
var propSetting = require('../config/propSetting');

//mongoose declaration
this.giswisata_schema = new mongoose.Schema({
  nama_tempat : String,
  daerah : String,
  kota : String,
  deskripsi : String,
  langtitude : String,
  longtitude : String,
  image_name : String
});

this.useradmin_schema = new mongoose.Schema(
{
	user_name : String,
	password : String
});

this.giswisata_entity = mongoose.model('giswisata',this.giswisata_schema);
this.useradmin_entity = mongoose.model('useradmin',this.useradmin_schema);
mongoose.connect(propSetting.getSetting.DB_URL);

