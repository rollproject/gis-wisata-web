
/**
 * Module dependencies.
 */

var express = require('express')
  , propSetting = require('./config/propSetting')
  , routes = require('./routes')
  , user = require('./routes/user')
  , giswisata = require('./routes/giswisata')
  , admin = require('./routes/admin')
  , http = require('http')
  //, db = require('./model/mgiswisata')
  , path = require('path');

var app = express();
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
// all environments
app.set('port', propSetting.getSetting.PORT_ACCESS); //process.env.OPENSHIFT_NODEJS_PORT || 8080
app.set('ipaddress', propSetting.getSetting.URL_ACCESS); 
//app.set('port', process.env.PORT || 3000);
//app.set('views', __dirname + '/views');
//app.set('view engine', 'jade');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser(
    {
         keepExtensions: true, uploadDir: __dirname + "/public/image_map"
    }));
//app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('56'));
app.use(express.cookieSession());
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//app.get('/', routes.index);
app.get('/', giswisata.home);
app.get('/admin', admin.home);
app.get('/users', user.list);
app.post('/saveuseradmin', admin.saveUserAdmin);
app.post('/savegiswisata', giswisata.saveGiswisata);
app.post('/imageupload', giswisata.imageupload);
app.get('/listgiswisata', giswisata.getAll_giswisata);
app.post('/deleteuseradmin', admin.delUserAdmin);
app.get('/listuseradmin', admin.getListUserAdmin);
app.get('/login', admin.loginSetup);
app.post('/dologin',admin.doLoginUser);
app.post('/deletewisata', giswisata.deletegiswisata);
app.get('/logout', admin.logoutUser);

/*
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});*/ //punya local

http.createServer(app).listen(app.get('port'), app.get('ipaddress'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

/*http.createServer(app).listen(app.get('port'), app.get('ipaddress'), function(){
  console.log("Express server listening on port " + app.get('port'));
});*/
