var fs = require('fs');
var gis_model = require('../model/mgiswisata');
//var request = require('request'); //fungsi untuk mengambil data di web lain dari server to server


exports.home = function(req, res){
    if (!req.session.user_id) {
        res.writeHead(301,
                {Location: '/login'});
        res.end();
    }else{
     res.render('map_home.html');
    }
};

exports.imageupload = function (req, res) {
    //console.log("blablabla",req.body.image_name);
    var bb = JSON.stringify(req.files.image_name);
    var cc = JSON.stringify(req.files.image_name);
    var kk = JSON.parse(cc);
    console.log("ini dia filenya", JSON.stringify(req.files.image_name));

    for (var a in kk) {
        var cc = kk[a];
        for (var b in cc) {
            console.log('bbabababa', cc[b].path);
            if (cc[b].size > 0) {
                var temp_path = cc[b].path;
                // console.log(temp_path);
                var datetime = new Date();
                var save_path = './public/image_map/' + "image_no_" + datetime+ cc[b].name
                console.log("blablabla", "image_no_" + datetime+ cc[b].name);
                fs.rename(temp_path, save_path, function (error) {
                    if (error) throw error;
                    fs.unlink(temp_path, function () {
                        if (error) throw error;
                        res.send("image_no_" + datetime+ cc[b].name);

                    });
                });
                //done(); 
                break;
            }
        }
    }


};

exports.saveGiswisata = function (req, res) {

    gis_model.getFindById_giswisata((req.body._id == null ? "" : req.body._id), function (data) {
        if (data) {
           console.log('yes is update',data);
           updategiswisata(req, data, res);
        } else {
            console.log('is update', data);
            var temp_gis = {
                nama_tempat: req.body.nama_tempat,
                daerah: req.body.daerah,
                kota: req.body.kota,
                deskripsi: req.body.deskripsi,
                langtitude: req.body.langtitude,
                longtitude: req.body.longtitude,
                image_name: req.body.image_name
            }

            console.log(temp_gis);
            res.send(temp_gis);

            gis_model.getSave_giswisata(temp_gis, function (callback) {
                if (callback) {
                    console.log('get callback', callback);
                    res.send(callback);
                } else {
                    res.send('cannot save');
                }
            });
        }
    });
}

exports.getUpdate_giswisata = function (req, res) {

    gis_model.getFindById_giswisata(req.body._id == null ? "" : req.body._id, function (callback) {

        if (callback) {
            updateFlight(req,callback, res);
        } else {
            console.log('status update', "not found");
            res.send("status update : not found");
        }
    });
}

updategiswisata = function(req,data,res){
    console.log('got it',data[0]._id);
    
    var temp_gis = {
        _id : req.body._id,
        nama_tempat : req.body.nama_tempat,
        daerah : req.body.daerah,
        kota : req.body.kota,
        deskripsi : req.body.deskripsi,
        langtitude : req.body.langtitude,
        longtitude : req.body.longtitude,
        image_name : req.body.image_name
    }
                
    gis_model.getUpdate_giswisata(temp_gis,function(callback){
        res.send(callback);
    });
      
};

exports.deletegiswisata = function(req,res){
    var isDelSuccess = false;
    //var id_del = JSON.parse(req.body.id_del);
    var id_del = JSON.parse(req.body._id);
    console.log((id_del));
    for(var a = 0; a< id_del.length ; a++){
        console.log("kena disini",id_del[a]._id);
        gis_model.getDel_giswisata(id_del[a]._id,function(callback){
            if (callback) {
               isDelSuccess = true;
            }else{
               isDelSuccess = false;
            }
        })
    }
    res.send(isDelSuccess);  
}


exports.getAll_giswisata = function(req,res){
    
    gis_model.getAll_giswisata(function(data){
        if (data) {
                res.header("Access-Control-Allow-Origin", req.headers.origin);
                res.writeHead(200, {"Content-Type": "application/json"});
                res.write(JSON.stringify(data));
                res.end();  
        }else{
            if (!data) res.writeHead(500, err.message)
            if (!data.length) res.writeHead(404);
        }
    });
}