var useradmin_db = require('../model/db');


exports.getSave_useradmin = function setSave_userAdmin(data, callback) {

    var user_temp = {
        user_name : data.user_name,
        password : data.password
        
    };

    var _mguseradmin = new useradmin_db.useradmin_entity(user_temp);

    _mguseradmin.save(function (error, data) {
        if (error) {
            callback(error);
        } else {
            console.log("Added New User Admin");
            callback(data);
        }
    });
};

exports.getFindById_userAdmin = function setFindById_userAdmin(parseData, callback) {
    var _mguseradmin = useradmin_db.useradmin_entity;
    _mguseradmin.find({ _id: parseData }, function (error, data) {
        if (data == '[]') {
            callback('not found');
        } else {
            callback(data);
        }
    });
}

exports.getFindById_userAdminLogin = function setFindById_userAdminLogin(parseData, parseDataPassword, callback) {
    var _mguseradmin = useradmin_db.useradmin_entity;
    _mguseradmin.find({$and:[{ user_name: parseData },{password:parseDataPassword}]}, function (error, data) {
        if (data == '[]') {
            callback('not found');
        } else {
            callback(data);
        }
    });
}

exports.getUpdate_useradmin = function setUpdate_useradmin(data, callback) {

    var user_temp = {
        user_name : data.user_name,
        password : data.password
        
    };
    var _mguseradmin = useradmin_db.useradmin_entity; //kalo update jangan pake new
   _mguseradmin.update({_id:data._id},user_temp,{upsert: true},function(error){
        callback(error);
   });

};


exports.getAll_userAdmin = function setAll_userAdmin(callback){
    var _mguseradmin = useradmin_db.useradmin_entity;//new doa_wanita.doa_wanita_entity(doa_wanita_data);
    _mguseradmin.find({},function (error, data) {
            if (data) {
                console.log(data);
                callback(data)
            }else{
                callback(null);
                data = null;
            }
        });
}

exports.getDel_userAdmin = function setDel_userAdmin(parseData,callback){
    var _mguseradmin = useradmin_db.useradmin_entity;
    _mguseradmin.find({_id: parseData}, function (error,data) {
        if (data) {
            console.log('ini dia',parseData)
            _mguseradmin.remove({_id:parseData},function(error){
                if (!error) {
                    console.log("delete data berhasil",error);
                    callback(error);
                } else {
                    console.log("delete data tidak berhasil",error);
                    callback(error);
                }
            })
        }   
    });
}