$( document ).ready(function() {
  // Handler for .ready() called.
    var gis_db = null;
    $('#gis-login-form').submit(function(e){
        var postData = $(this).serializeArray();
        $.ajax({
            url: '/dologin',
            type: 'POST',
            dataType: 'JSON',
            data: postData,
            success: function (data, textStatus, jqXHR) {
                
            },error: function (jqXHR, textStatus, errorThrown) {
                console.log("datanya",jqXHR);
                if(jqXHR.responseText == "login unsuccessfully"){ 
                    $(".dialog-popup").css({"display":"block"});
                }else if(jqXHR.responseText == "login successfully"){
                    window.location.href = '/';
                }
            }        
        });
        e.preventDefault();
    });
    
     $('#gis-useradmin-form').submit(function(e){
         if($("#password").text == $("#retypepassword").text){
            var postData = $(this).serializeArray(); 
             $.ajax({
                url: '/saveuseradmin',
                type: 'POST',
                dataType: 'JSON',
                data: postData,
                success: function (data, textStatus, jqXHR) {
                    window.location.href = '/admin';
                },error: function (jqXHR, textStatus, errorThrown) {
                    $(".dialog-popup").css({"display":"block"});
                }        
            });
            e.preventDefault();
         }
     });
    
    $(".btnTryAgain").click(function(e){
            $(".dialog-popup").css({"display":"none"});
    });
    
    $(".btnDelete").click(function(e){
        var postData = {
            _id : $('input[name="_id"]').val()
        }
        
         $.ajax({
                url: '/deleteuseradmin',
                type: 'POST',
                dataType: 'JSON',
                data: postData,
                success: function (data, textStatus, jqXHR) {
                    window.location.href = '/admin';
                },error: function (jqXHR, textStatus, errorThrown) {
                    $(".dialog-popup").css({"display":"block"});
                }        
            });
    });
    
    
    /* list user admin */
    
    var raw_list = $("#list-raw").html();

    draw_data_wisata = function (_id, user_name,index) {
        temp_raw = raw_list;
        temp_raw = temp_raw.replace(/(\{_id\})/g, _id).replace(/(\{user_name\})/g, user_name).replace(/(\{index\})/g, index);
        return temp_raw;
    }
    
    setlistdatauseradmin = function () {
        $.ajax({
            url: '/listuseradmin',
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
                console.log(data);
                html = '';
                gis_db = data;
                for (var i in data) {
                    html += draw_data_wisata(data[i]._id, data[i].user_name, i);

                    //console.log(html);
                }
                $("#gis-useradminlist-container").html(html);

            },
            error: function (data) {

            }
        });
    }
    
    setlistdatauseradmin();
    
    openUserName = function(index,thisobject){
        $('input[name="_id"]').val(gis_db[index]._id);
        $('input[name="username"]').val(gis_db[index].user_name);
        $('input[name="password"]').val(gis_db[index].password);
        $('input[name="retypepassword"]').val(gis_db[index].password);
    }
    
});