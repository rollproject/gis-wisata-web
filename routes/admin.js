var fs = require('fs');
var gis_model = require('../model/museradmin');

exports.home = function(req, res){
     if (!req.session.user_id) {
        res.writeHead(301,
                {Location: '/login'});
        res.end();
    }else{
        res.render('admin.html');
    }
};

exports.saveUserAdmin = function (req, res) { 
		gis_model.getFindById_userAdmin((req.body._id == null ? "" : req.body._id), function (data) {
        if (data) {
           console.log('yes is update',data);
           updateuseradmin(req, data, res);
        } else {
            console.log('is update', data);
            var temp_useradmin = {
                user_name: req.body.username,
                password: req.body.password
            }

            console.log(temp_useradmin);
            res.send(temp_useradmin);

            gis_model.getSave_useradmin(temp_useradmin, function (callback) {
                if (callback) {
                    console.log('get callback', callback);
                    res.send(callback);
                } else {
                    res.send('cannot save');
                }
            });
        }
    });
}

updateuseradmin = function (req,  data, res) { 
	//console.log('got it',data[0]._id);
    console.log("idnya",req.body._id);
    var temp_useradmin = {
        _id : req.body._id,
        user_name: req.body.username,
        password: req.body.password
        
    }
                
    gis_model.getUpdate_useradmin(temp_useradmin,function(callback){
        res.send(callback);
    });
}

exports.getListUserAdmin = function (req, res) { 
	gis_model.getAll_userAdmin(function(data){
        if (data) {
                res.header("Access-Control-Allow-Origin", req.headers.origin);
                res.writeHead(200, {"Content-Type": "application/json"});
                res.write(JSON.stringify(data));
                res.end();  
        }else{
            if (!data) res.writeHead(500, err.message)
            if (!data.length) res.writeHead(404);
        }
    });
}

exports.delUserAdmin = function (req, res) { 
	var isDelSuccess = false;
    gis_model.getDel_userAdmin(req.body._id,function(callback){
        if (callback) {
           isDelSuccess = true;
        }else{
           isDelSuccess = false;
        }
    })
    res.send(isDelSuccess);  
}

exports.loginSetup = function(req,res){
    res.render("login.html");
}

exports.doLoginUser = function(req,res){
    console.log("username",req.body.username);
    console.log("password",req.body.password);
    gis_model.getFindById_userAdminLogin((req.body.username == null ? "" : req.body.username),(req.body.password == null ? "" : req.body.password), function (data) {
        if (data) {
            if(data == ""){
                console.log('no login', data); 
                res.send("login unsuccessfully");
            }else{
                req.session.user_id = req.body.username;
                console.log('yes is login',data);
                console.log("header post",req.headers.host); 
                res.send("login successfully");
            }
            
            /*res.writeHead(301,
                {Location: 'http://whateverhostthiswillbe:8675/'+newRoom});
            res.end();*/
            
        } else {
            console.log('no login', data); 
            res.send("login unsuccessfully");
        }
    });
}

exports.logoutUser = function(req,res){
    delete req.session.user_id;
    res.redirect('/login');
}